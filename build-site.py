#!/usr/bin/env python3

import datetime
import functools
import os
import sys
import requests
from jinja2 import Template


PUBLIC_DIR = "public"
GITLAB_HOSTNAME = "gitlab.com"
REGISTRY_HOSTNAME = "registry.gitlab.com"
DOCKER_IMAGE = "datadrivendiscovery/images/primitives"


@functools.lru_cache(maxsize=None)
def get_metadata(tag, token):
    response = requests.get(
        'https://{REGISTRY_HOSTNAME}/v2/{DOCKER_IMAGE}/manifests/{tag}'.format(
            REGISTRY_HOSTNAME=REGISTRY_HOSTNAME,
            DOCKER_IMAGE=DOCKER_IMAGE,
            tag=tag,
        ),
        headers={
            'Authorization': 'Bearer {token}'.format(token=token),
        },
    )
    response.raise_for_status()

    digest = response.json()['config']['digest']

    response = requests.get(
        'https://{REGISTRY_HOSTNAME}/v2/{DOCKER_IMAGE}/blobs/{digest}'.format(
            REGISTRY_HOSTNAME=REGISTRY_HOSTNAME,
            DOCKER_IMAGE=DOCKER_IMAGE,
            digest=digest,
        ),
        headers={
            'Authorization': 'Bearer {token}'.format(token=token),
        },
    )
    response.raise_for_status()

    labels = response.json()['config']['Labels']

    commit = labels.get('org.datadrivendiscovery.public.primitives-commit', None)
    timestamp = labels.get('org.datadrivendiscovery.public.timestamp', None)

    if commit or timestamp:
        return {
            'commit': commit,
            'timestamp': timestamp,
        }

    return None


def get_commit(tag, token):
    result = get_metadata(tag, token)

    if result:
        return result['commit'] or None

    return None


def get_timestamp(tag, token):
    result = get_metadata(tag, token)

    if result:
        return result['timestamp'] or None

    return None


def render_timestamp(timestamp):
    if not timestamp:
        return timestamp

    return str(datetime.datetime.strptime(timestamp, '%Y%m%d-%H%M%S'))


def get_docker_images():
    response = requests.get(
        'https://{GITLAB_HOSTNAME}/jwt/auth?service=container_registry&scope=repository:{DOCKER_IMAGE}:pull'.format(
            GITLAB_HOSTNAME=GITLAB_HOSTNAME,
            DOCKER_IMAGE=DOCKER_IMAGE,
        ),
    )
    response.raise_for_status()
    token = response.json()['token']

    response = requests.get(
        'https://{REGISTRY_HOSTNAME}/v2/{DOCKER_IMAGE}/tags/list'.format(
            REGISTRY_HOSTNAME=REGISTRY_HOSTNAME,
            DOCKER_IMAGE=DOCKER_IMAGE,
        ),
        headers={
            'Authorization': 'Bearer {token}'.format(token=token),
        },
    )
    response.raise_for_status()
    result = response.json()

    images = {}
    for tag in result['tags']:
        try:
            system, release, python, image_type, timestamp = tag.split('-', 4)
        except ValueError:
            system, release, python, image_type = tag.split('-', 3)
            timestamp = ''

        base = '-'.join([system, release, python])
        if base not in images:
            images[base] = {
                'system': system,
                'release': release,
                'python': python,
                'image_types': {},
            }

        if image_type not in images[base]['image_types']:
            images[base]['image_types'][image_type] = {
                'timestamps': [],
            }

        images[base]['image_types'][image_type]['timestamps'].append({
            'timestamp': timestamp,
        })

    flatten = []
    for base_key, base_value in images.items():
        for image_type_key, image_type_value in base_value['image_types'].items():
            # "z" character is after all timestamps (which start with a digit).
            timestamps = sorted(image_type_value['timestamps'], key=lambda v: v['timestamp'] or 'z', reverse=True)
            image_id = '{base_key}-{image_type_key}'.format(base_key=base_key, image_type_key=image_type_key)

            for timestamp in timestamps:
                if timestamp['timestamp']:
                    tag = '{image_id}-{timestamp}'.format(image_id=image_id, timestamp=timestamp['timestamp'])
                else:
                    tag = image_id
                    timestamp['timestamp'] = get_timestamp(tag, token)

                timestamp['timestamp_rendered'] = render_timestamp(timestamp['timestamp'])

                commit = get_commit(tag, token)
                timestamp['tag'] = tag
                timestamp['full_name'] = '{REGISTRY_HOSTNAME}/{DOCKER_IMAGE}:{tag}'.format(
                    REGISTRY_HOSTNAME=REGISTRY_HOSTNAME,
                    DOCKER_IMAGE=DOCKER_IMAGE,
                    tag=tag,
                )
                if commit:
                    timestamp['commit'] = commit[:8]
                    timestamp['commit_url'] = 'https://gitlab.com/datadrivendiscovery/primitives/tree/' + commit

            flatten.append({
                # Making sure it is a valid ID.
                'image_id': image_id.replace('.', '-'),
                'system': base_value['system'],
                'release': base_value['release'],
                'python': base_value['python'],
                'image_type': image_type_key,
                'timestamps': timestamps,
                'timestamps_number': len(timestamps),
                'latest_timestamp': timestamps[0]['timestamp'],
                'latest_timestamp_rendered': render_timestamp(timestamps[0]['timestamp']),
            })

    flatten = sorted(flatten, key=lambda v: v['latest_timestamp'], reverse=True)

    return flatten


def main(args):
    with open('templates/index.template') as file_:
        template = Template(file_.read())
    index_html = template.render()
    if not os.path.exists(PUBLIC_DIR):
        os.mkdir(PUBLIC_DIR)
    with open(os.path.join(PUBLIC_DIR, "index.html"), 'w') as fw:
        fw.write(index_html)

    with open('templates/docker.template') as file_:
        template = Template(file_.read())
    index_html = template.render(docker_images=get_docker_images())
    if not os.path.exists(PUBLIC_DIR):
        os.mkdir(PUBLIC_DIR)
    with open(os.path.join(PUBLIC_DIR, "docker.html"), 'w') as fw:
        fw.write(index_html)

    versions_file = args[0]
    versions = list()
    with open(versions_file) as fr:
        for line in fr:
            versions.append(line.strip("\n"))
    print("Found {} versions.".format(versions))
    if len(versions) < 1:
        print("No versions found.")
    with open('templates/core.template') as file_:
        template = Template(file_.read())
    index_html = template.render(versions_list=versions)
    if not os.path.exists(PUBLIC_DIR):
        os.mkdir(PUBLIC_DIR)
    with open(os.path.join(PUBLIC_DIR, "core.html"), 'w') as fw:
        fw.write(index_html)


if __name__ == '__main__':
    main(sys.argv[1:])
